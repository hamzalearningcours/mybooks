<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\Status;
use App\Entity\UserBook;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        // Create 10 users
        $users = [];
        for ($i = 0; $i < 10; ++$i) {
            $user = new Users();
            $user->setEmail($faker->email())
                ->setPassword($faker->password(6, 8))
                ->setPseudo($faker->name());
            $manager->persist($user);
            $users[] = $user;
        }

        // Create 10 Author
        $authors = [];
        for ($i = 0; $i < 10; ++$i) {
            $author = new Author();
            $author->setName($faker->name());
            $manager->persist($author);
            $authors[] = $author;
        }
        // Create Status
        $statuss = [];
        foreach (['to-read', 'reading', 'read'] as $val) {
            $status = new Status();
            $status->setName($val);
            $manager->persist($status);
            $statuss[] = $status;
        }
        // Create 10 Publishers
        $publishers = [];
        for ($i = 0; $i < 10; ++$i) {
            $publisher = new Publisher();
            $publisher->setName($faker->company);
            $manager->persist($publisher);
            $publishers[] = $publisher;
        }

        // Create 100 Books
        $books = [];
        for ($i = 0; $i < 100; ++$i) {
            $book = new Book();

            /** @phpstan-ignore-next-line */
            $isbn10 = $faker->isbn10;

            /** @phpstan-ignore-next-line */
            $isbn13 = $faker->isbn13;

            $book
                ->setGoogleBooksId($faker->uuid)
                ->setTitle($faker->sentence)
                ->setSubtitle($faker->sentence)
                ->setPublishDate(\DateTimeImmutable::createFromMutable($faker->dateTime))
                ->setDescription($faker->text)
                ->setIsbn10($isbn10)
                ->setIsbn13($isbn13)
                ->setPageCount($faker->numberBetween(100, 1000))
                ->setThumbnail('https://picsum.photos/200/300')
                ->setSmallThumbnail('https://picsum.photos/100/150')
                ->addAuthor($faker->randomElement($authors))
                ->addPublisher($faker->randomElement($publishers));
            $manager->persist($book);

            $books[] = $book;
        }

        // Create 10 UserBook by User
        foreach ($users as $user) {
            for ($i = 0; $i < 10; ++$i) {
                $userBook = new UserBook();
                $userBook
                    ->setUsers($user)
                    ->setStatus($faker->randomElement($statuss))
                    ->setRating($faker->numberBetween(0, 5))
                    ->setComment($faker->text)
                    ->setBook($faker->randomElement($books))
                    ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime))
                    ->setUpdateAt(\DateTimeImmutable::createFromMutable($faker->dateTime));

                $manager->persist($userBook);
            }
        }

        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
