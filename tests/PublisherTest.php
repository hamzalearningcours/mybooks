<?php

namespace App\Tests;

use App\Entity\Book;
use App\Entity\Publisher;
use PHPUnit\Framework\TestCase;

class PublisherTest extends TestCase
{
    public function testIsTrue(): void
    {
        $publisher = new Publisher();
        $book = new Book();
        $publisher->setName('publisher')
               ->addBook($book);

        $this->assertTrue($publisher->getName()==='publisher');
        $this->assertContains($book, $publisher->getBooks());
    }

    public function testIsFalse(): void
    {
        $publisher = new Publisher();
        $book = new Book();
        $publisher->setName('publisher')
               ->addBook($book);

        $this->assertFalse($publisher->getName()==='publisherfase');
        $this->assertNotContains(new Book(), $publisher->getBooks());
    }

    public function testIsEmpty(): void
    {
        $publisher = new Publisher();

        $this->assertEmpty($publisher->getName());
        $this->assertEmpty($publisher->getBooks());
    }
}
