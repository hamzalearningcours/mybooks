<?php

namespace App\Tests;


use App\Entity\Status;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    public function testIsTrue(): void
    {
        $status = new Status();
        $userbook = new UserBook();
        $status->setName('status')
               ->addUserBook($userbook);

        $this->assertTrue($status->getName()==='status');
        $this->assertContains($userbook, $status->getUserBooks());
    }

    public function testIsFalse(): void
    {   
        $status = new Status();
        $userbook = new UserBook();
        $status->setName('status')
               ->addUserBook($userbook);

        $this->assertFalse($status->getName()==='statusfalse');
        $this->assertNotContains(new UserBook(), $status->getUserBooks());

    }

    public function testIsEmpty(): void
    {
        $status = new Status();

        $this->assertEmpty($status->getName());
        $this->assertEmpty($status->getUserBooks());
    }
}
