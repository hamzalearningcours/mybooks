<?php

namespace App\Tests;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class BookUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $book = new Book();
        $userbook = new UserBook();
        $publisher = new Publisher();
        $author = new Author();
        $datepublisher = new \DateTimeImmutable();
        $book->setGoogleBooksId("googlebooksid")
            ->setTitle('booktitle')
            ->setSubtitle('booksubtitle')
            ->setPublishDate($datepublisher)
            ->setDescription('description')
            ->setIsbn10('isbn10')
            ->setIsbn13('isbn13')
            ->setPageCount(10)
            ->setSmallThumbnail('smallThumbnail')
            ->setThumbnail('thumbnail')
            ->addAuthor($author)
            ->addPublisher($publisher)
            ->addUserBook($userbook);
        
        $this->assertTrue($book->getGoogleBooksId()==='googlebooksid');
        $this->assertTrue($book->getTitle()==='booktitle');
        $this->assertTrue($book->getSubtitle()==='booksubtitle');
        $this->assertTrue($book->getPublishDate()===$datepublisher);
        $this->assertTrue($book->getDescription()==='description');
        $this->assertTrue($book->getIsbn10()==='isbn10');
        $this->assertTrue($book->getIsbn13()==='isbn13');
        $this->assertTrue($book->getPageCount()=== 10 );
        $this->assertTrue($book->getSmallThumbnail()==='smallThumbnail');
        $this->assertTrue($book->getThumbnail()==='thumbnail');
        $this->assertContains($author, $book->getAuthor());
        $this->assertContains($publisher, $book->getPublisher());
        $this->assertContains($userbook, $book->getUserBooks());
    }

    public function testIsFalse(): void
    {
        $book = new Book();
        $userbook = new UserBook();
        $publisher = new Publisher();
        $author = new Author();
        $datepublisher = new \DateTimeImmutable();

        $book->setGoogleBooksId("googlebooksid")
            ->setTitle('booktitle')
            ->setSubtitle('booksubtitle')
            ->setPublishDate($datepublisher)
            ->setDescription('description')
            ->setIsbn10('isbn10')
            ->setIsbn13('isbn13')
            ->setPageCount(10)
            ->setSmallThumbnail('smallThumbnail')
            ->setThumbnail('thumbnail')
            ->addAuthor($author)
            ->addPublisher($publisher)
            ->addUserBook($userbook);
        
        $this->assertFalse($book->getGoogleBooksId()==='googlebooksidfalse');
        $this->assertFalse($book->getTitle()==='booktitlefalse');
        $this->assertFalse($book->getSubtitle()==='booksubtitlefalse');
        $this->assertFalse($book->getPublishDate()===new \DateTimeImmutable());
        $this->assertFalse($book->getDescription()==='descriptionfalse');
        $this->assertFalse($book->getIsbn10()==='isbn10false');
        $this->assertFalse($book->getPageCount()=== 20 );
        $this->assertFalse($book->getSmallThumbnail()==='smallThumbnailfalse');
        $this->assertFalse($book->getThumbnail()==='thumbnailfalse');
        $this->assertNotContains(new Author(), $book->getAuthor());
        $this->assertNotContains(new Publisher(), $book->getPublisher());
        $this->assertNotContains(new UserBook(), $book->getUserBooks());
    }

    public function testIsEmpty(): void
    {
        $book = new Book();
        $this->assertEmpty($book->getGoogleBooksId());
        $this->assertEmpty($book->getTitle());
        $this->assertEmpty($book->getSubtitle());
        $this->assertEmpty($book->getPublishDate());
        $this->assertEmpty($book->getDescription());
        $this->assertEmpty($book->getIsbn10());
        $this->assertEmpty($book->getPageCount());
        $this->assertEmpty($book->getSmallThumbnail());
        $this->assertEmpty($book->getThumbnail());
        $this->assertEmpty($book->getAuthor());
        $this->assertEmpty($book->getPublisher());
        $this->assertEmpty($book->getUserBooks());
    }
}
