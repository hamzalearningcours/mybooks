<?php

namespace App\Tests;

use App\Entity\Book;
use App\Entity\Author;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    public function testIsTrue(): void
    {
        $author = new Author();
        $book = new Book();
        $author->setName('author')
               ->addBook($book);

        $this->assertTrue($author->getName()==='author');
        $this->assertContains($book, $author->getBooks());
    }

    public function testIsFalse(): void
    {
        $author = new Author();
        $book = new Book();
        $author->setName('author')
               ->addBook($book);

        $this->assertFalse($author->getName()==='authorfase');
        $this->assertNotContains(new Book(), $author->getBooks());
    }

    public function testIsEmpty(): void
    {
        $author = new Author();

        $this->assertEmpty($author->getName());
        $this->assertEmpty($author->getBooks());
    }

}
