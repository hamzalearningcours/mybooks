<?php

namespace App\Tests;

use App\Entity\Users;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class UsersTest extends TestCase
{
    public function testIsTrue(): void
    {
        $users = new Users();
        $userbook = new UserBook();

        $users->setEmail('email@email.com')
              ->setPassword('password')
              ->setPseudo('pseudo')
              ->addUserBook($userbook);

        $this->assertTrue($users->getEmail()==='email@email.com');
        $this->assertTrue($users->getPassword()==='password');
        $this->assertTrue($users->getPseudo()==='pseudo');
        $this->assertContains($userbook, $users->getUserBooks());
    }

    public function testIsFalse(): void
    {
        $users = new Users();
        $userbook = new UserBook();

        $users->setEmail('email@email.com')
              ->setPassword('password')
              ->setPseudo('pseudo')
              ->addUserBook($userbook);

        $this->assertFalse($users->getEmail()==='false@email.com');
        $this->assertFalse($users->getPassword()==='false');
        $this->assertFalse($users->getPseudo()==='false');
        $this->assertNotContains(new UserBook(), $users->getUserBooks());
    }

    public function testIsEmpty(): void
    {
        $users = new Users();

        $this->assertEmpty($users->getEmail());
        $this->assertEmpty($users->getPassword());
        $this->assertEmpty($users->getPseudo());
        $this->assertEmpty($users->getUserBooks());
    }
}
