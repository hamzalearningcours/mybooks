<?php

namespace App\Tests;

use App\Entity\Book;
use App\Entity\Status;
use App\Entity\UserBook;
use App\Entity\Users;
use PHPUnit\Framework\TestCase;

class UserBookTest extends TestCase
{
    public function testIsTrue(): void
    {
        $userbook = new UserBook();
        $createdat = new \DateTimeImmutable();
        $updateat = new \DateTimeImmutable();
        $users = new Users();
        $book = new Book();
        $status = new Status();

        $userbook->setCreatedAt($createdat)
                ->setUpdateAt($updateat)
                ->setComment('Comment')
                ->setRating(5)
                ->setUsers($users)
                ->setBook($book)
                ->setStatus($status);

        $this->assertTrue($userbook->getCreatedAt()===$createdat);
        $this->assertTrue($userbook->getUpdateAt()===$updateat);
        $this->assertTrue($userbook->getComment()==='Comment');
        $this->assertTrue($userbook->getRating()===5);
        $this->assertTrue($userbook->getBook()===$book);
        $this->assertTrue($userbook->getUsers()===$users);
        $this->assertTrue($userbook->getStatus()===$status);
    }

    public function testIsFalse(): void
    {
        $userbook = new UserBook();
        $createdat = new \DateTimeImmutable();
        $updateat = new \DateTimeImmutable();
        $users = new Users();
        $book = new Book();
        $status = new Status();

        $userbook->setCreatedAt($createdat)
                ->setUpdateAt($updateat)
                ->setComment('Comment')
                ->setRating(5)
                ->setUsers($users)
                ->setBook($book)
                ->setStatus($status);

        $this->assertFalse($userbook->getCreatedAt()===new \DateTimeImmutable());
        $this->assertFalse($userbook->getUpdateAt()===new \DateTimeImmutable());
        $this->assertFalse($userbook->getComment()==='Commentfalse');
        $this->assertFalse($userbook->getRating()===3);
        $this->assertFalse($userbook->getBook()===new Book());
        $this->assertFalse($userbook->getUsers()===new Users());
        $this->assertFalse($userbook->getStatus()===new Status());
    }

    public function testIsEmpty(): void
    {
        $userbook = new UserBook();

        $this->assertEmpty($userbook->getCreatedAt());
        $this->assertEmpty($userbook->getUpdateAt());
        $this->assertEmpty($userbook->getComment());
        $this->assertEmpty($userbook->getRating());
        $this->assertEmpty($userbook->getBook());
        $this->assertEmpty($userbook->getUsers());
        $this->assertEmpty($userbook->getStatus());
    }
}
